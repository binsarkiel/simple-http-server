// Import HTTP Module
const http = require("http")
// Import File System Module
const fs = require("fs")

// fungsi untuk menghandle request
// dari user ...
function handleRequest(req, res) {
    // logging request yang dikirim oleh user
    console.log(req.method, req.url)

    // pengecekan url
    if (req.url == "/person") {

        switch(req.method) {
            case "GET":
                // jika sudah ada file person.json maka tampilkan
                // selain itu kirim response 404 (not found)
                if (fs.existsSync("./person.json")) {
                    let person = fs.readFileSync("./person.json")

                    res.writeHead(200, {'content-type': 'application/json'})
                    res.end(person.toString())
                    return
                }
                res.writeHead(404, {'content-type': 'application/json'})
                res.end(`{"error": "not found"}`)
            break;
            case "POST":
                // jika belum ada file person.json maka buat dan isi
                // selain itu kirim response 409 (duplicate)
                if (!fs.existsSync("./person.json")) {
                    let personString = ""
                    req.on("data", function(body) {
                        personString = body.toString()
                    })
                    req.on("end", function() {
                        // write file ...
                        fs.writeFileSync("./person.json", personString)

                        res.writeHead(200, {'content-type': 'application/json'})
                        res.end(personString)
                    })
                    return
                }

                res.writeHead(409, {'content-type': 'application/json'})
                res.end(`{"error": "person already exists"}`)
            break;
            case "PUT":
                // jika sudah ada file person.json maka perbarui
                // dan berikan response 200 (ok)
                // selain itu 404 (not found)
                res.end("PUT Method")
            break;
            case "DELETE":
                // jika sudah ada file person.json maka hapus file tsb
                // selain itu kirim response 404 (not found)
                res.end("DELETE Method")
            break;
            default:
                res.writeHead(404)
                res.end("Not found")
            break;
        }

        return
    }

    // jika url selain /person dinyatakan sebagai not found
    res.writeHead(404)
    res.end("Not found")
}

// Persiapkan HTTP server
const server = http.createServer(handleRequest)

// Jalankan HTTP Server
// Diusahakan port > 1000 untuk development di local
server.listen(3000, function() {
    console.log("Server running on port 3000")
})

// buka localhost:PORT, misal http://localhost:3000/hello
// http://123.123.123.123:3000